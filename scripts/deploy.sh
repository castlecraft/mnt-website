#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls --namespace mntechnique -q | grep mnt-web)
if [ "$CHECK_WEB" = "mnt-web" ]
then
    echo "Updating existing mnt-web . . ."
    helm upgrade mnt-web \
        --namespace mntechnique \
        --reuse-values \
        helm-chart/mnt-web
fi
